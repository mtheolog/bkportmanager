
# bkPortManager

It's a background application that supports two fold communication:
-	Connects to CMGs (via USB or UART) to receive data and forward cloud commands 
-	Connects to bkIPC_AppService to send to and receive cloud commands from bkNstApplication

> During operation, Gateway.db (stored in c:\Database folder) is read to obtain neccessary information

## Hardware requirements

** Raspberry Pi 3, Dragonboard 410c **


## Operating system requirements

** Windows 10 IoT Core ** 


## Nuget Package requirements

-	Microsoft.Data.Sqlite ver 1.1.1
-	Microsoft.NETCore.UniversalWindowsPlatform ver 5.2.2

## Build the sample

1. Start Microsoft Visual Studio 2017 and select **File** \> **Open** \> **Project/Solution**.
2. Starting in the "Middleware" folder, go to the bkCloudManager subfolder. Double-click the Visual Studio 2017 Solution (.sln) file.
3. In solution configuration, select "Debug", "ARM", "Remote Machine" and press "Find" to locate the connected device. 
4. Press Ctrl+Shift+B, or select **Build** \> **Build Solution**.


## Licensing

"The code in this project is licensed under NST license."

﻿using System;
using Windows.ApplicationModel.Background;

using System.Diagnostics;
using Windows.ApplicationModel.AppService;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.System.Threading;
using Windows.Devices.SerialCommunication;
using Windows.Devices.Enumeration;
using Windows.Storage.Streams;
using System.Threading;
using System.Threading.Tasks;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace bkPortManager
{
    public sealed class StartupTask : IBackgroundTask
    {

        private ThreadPoolTimer _timer;
        private BackgroundTaskDeferral _deferral;


        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Get a BackgroundTaskDeferral and hold it forever if initialization is sucessful.
            _deferral = taskInstance.GetDeferral();

            //AppServiceBridge.RequestReceived += AppServiceRequestHandler;
            await IPCservice.InitAsync();

            taskInstance.Canceled += (IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason) =>
            {
                Debug.WriteLine("Cancelled: reason " + reason);
                _deferral.Complete();
            };

            MemoryManager.AppMemoryUsageIncreased += MemoryManager_AppMemoryUsageIncreased;

            //_timer = ThreadPoolTimer.CreatePeriodicTimer(LogSensorDataAsync, TimeSpan.FromSeconds(90));
            //LogSensorDataAsync(null);


            ReadWrite.ConnectToSerialPort();
            // @@@@@@@@@@@@@@
           // ReadWrite.ConnectToSerialPort();
        }


        private void MemoryManager_AppMemoryUsageIncreased(object sender, object e)
        {
            var level = MemoryManager.AppMemoryUsageLevel;
            if (level != AppMemoryUsageLevel.Low)
            {
                Debug.WriteLine($"Memory limit {MemoryManager.AppMemoryUsageLevel} crossed: Current: {MemoryManager.AppMemoryUsage}, limit: {MemoryManager.AppMemoryUsageLimit}");
            }
        }


    }
}





﻿using Microsoft.Data.Sqlite;
using comNST;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using System.IO;

namespace bkPortManager
{
    class ReadWrite
    {

        // keeps the serial port of each linked CMG
        // contains CMG_UNIQUE_ID and SerialDevice sp
        private static Dictionary<string, SerialDevice> spPerCMG = new Dictionary<string, SerialDevice>();

        private static readonly SemaphoreSlim Semaphore = new SemaphoreSlim(1);

        static DataWriter dataWriterObject = null;
   
        static private CancellationTokenSource WriteCancellationTokenSource;
        static string[] CMGsDeclaredinDB = new string[8];
        static int numOfCMGsDeclaredinDB = 0;

        public Windows.Foundation.TypedEventHandler<SerialDevice, ErrorReceivedEventArgs> ErrorReceivedEventHandler;

        //-----------------Connect to Serial Port------------------------------------------------------------------
        public static async void ConnectToSerialPort()
        {
            try
            {
                SerialDevice sp = null;

                // Get from Gateway database, all CMG_IDs connected to Gateway
                CMGsDeclaredinDB = await dbGetAllCMGs();

                string allDevices = SerialDevice.GetDeviceSelector();
                DeviceInformationCollection dis = await DeviceInformation.FindAllAsync(allDevices);

                if (dis.Count == 0)
                {
                    Debug.WriteLine("No serial devices found...");
                    await Logging.WriteDebugLog("ConnectToSerialPort - {0}", "No serial devices found");
                    return; 
                }

                int numOfValidPorts = 0;
                for (int i = 0; i < dis.Count; i++)
                {
                    // Get all CMGs connected to Gateway                
                    DeviceInformation deviceInfo = dis[i];
                    //string code = deviceInfo.Id.Substring(4, 4);
                    //if (code.Equals("FTDI"))
                    //string code = deviceInfo.Id.Substring(29, 2);
                    //if (code.Equals("00"))

                    if (deviceInfo.Id.EndsWith("UART1"))
                    {
                        Debug.WriteLine("Found valid port: " + deviceInfo.Id);
                        await Logging.WriteDebugLog("Valid port found - {0}", deviceInfo.Id);
                        sp = await SerialDevice.FromIdAsync(deviceInfo.Id);

                        // add to dictionary the CMG_UNIQUE_ID and SerialDevice sp
                        //spPerCMG.Add(Convert.ToString(i), sp);
                        spPerCMG.Add(CMGsDeclaredinDB[numOfValidPorts], sp);
                        numOfValidPorts++;
                    }
                }

                if (numOfValidPorts != numOfCMGsDeclaredinDB)
                {
                    Debug.WriteLine("The number of CMGs declared in DB does not match with the number of CMGs physically connected in Gateway");
                    await Logging.WriteDebugLog("The number of CMGs declared in DB does not match with the number of CMGs physically connected in Gateway");
                }

                // Get only the serial ports  from spPerCMG
                Dictionary<string, SerialDevice>.ValueCollection spList = spPerCMG.Values;
                Task[] listen = new Task[spPerCMG.Count];
                int k = 0;
                foreach (SerialDevice s in spList)
                {
                    
                    s.WriteTimeout = TimeSpan.FromMilliseconds(1000);
                    s.ReadTimeout = TimeSpan.FromMilliseconds(1000);
                    s.BaudRate = 115200;
                    s.Parity = SerialParity.None;
                    s.StopBits = SerialStopBitCount.One;
                    s.DataBits = 8;
                    s.Handshake = SerialHandshake.None;
                    
                    s.ErrorReceived += portErrorReceived; // Add ErrorReceived Event Handler

                    // Listen each serial port as CMG_UNIQUE_ID
                    //listen[k] = Listen(s, Convert.ToString(k));
                    listen[k] = Listen(s, CMGsDeclaredinDB[k]);
                    k++;
                }
                for (int i = 0; i < spPerCMG.Count; i++)
                {
                    await listen[i];
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ConnectToSerialPort: Exception thrown - Message: " + ex.Message);
                //string exMessage = "ConnectToSerialPort: Exception thrown - Message: " + ex.Message + "\n";
                //exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                //exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                await Logging.WriteSystemLog("ConnectToSerialPort - {0}", ex.Message);
            }
        }

        private static async Task Listen(SerialDevice sp, string whoAmI)
        {
            try
            {
                if (sp != null)
                {
                    // Create cancellation token object to close I/O operations when closing the device
                    CancellationTokenSource ReadCancellationTokenSource = new CancellationTokenSource();

                    //SerialDevice xx = null;
                    //spPerCMG.TryGetValue("testCMG1", out xx);

                    await ReadAsync(ReadCancellationTokenSource.Token, new DataReader(sp.InputStream), whoAmI);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Listen: Exception thrown - Message: " + ex.Message);
                //string exMessage = "Listen: Exception thrown - Message: " + ex.Message + "\n";
                //exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                //exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                await Logging.WriteSystemLog("Listen - {0}", ex.Message);
            }
        }

        static string input;
       

        private static async Task ReadAsync(CancellationToken cancellationToken, DataReader dataReaderObject, string whoAmI)
        {

            Task<UInt32> loadAsyncTask;           
            uint ReadBufferLength = 35; // 35 = 2(responce code) + 16(token) + 4(IPnode) + 2(resource) + 8(value) + 1(newline)

            // If task cancellation was requested, complydataReaderObject
            cancellationToken.ThrowIfCancellationRequested();

            // Set InputStreamOptions to complete the asynchronous read operation when one or more bytes is available
            dataReaderObject.InputStreamOptions = InputStreamOptions.Partial;            

            //dataReaderObject.UnicodeEncoding = System.Text.UnicodeEncoding.Utf8;
            dataReaderObject.UnicodeEncoding = UnicodeEncoding.Utf8;

            // Create a task object to wait for data on the serialPort.InputStream
            while (true)
            {
                try
                {
                    loadAsyncTask = dataReaderObject.LoadAsync(ReadBufferLength).AsTask(cancellationToken);

                    // Launch the task and wait 
                    UInt32 bytesRead = await loadAsyncTask;

                    if (bytesRead > 0)
                    {
                        input = dataReaderObject.ReadString(bytesRead);

                        Debug.WriteLine("From: " + whoAmI + " data read: " + input);
                        await Logging.WriteDebugLog("From: {0} data read: {1}", whoAmI, input);

                        // BE CAREFUL FOR BUFFER SIZE
                        //ValueSet sentData = new ValueSet();

                        await Semaphore.WaitAsync();
                        try
                        {
                            Debug.WriteLine("before send to Nst App");
                            await sendToNstApp(input);
                            //sentData.Add("Message", input);
                            //sentData.Add("Receiver", "bkNstApplication");
                            //sentData.Add("Sender", "bkPortManager");de
                            //await IPCservice.SendMessageAsync(sentData);
                            //sentData.Clear();
                            Debug.WriteLine("after send to Nst App");
                        }
                        finally
                        {
                            Semaphore.Release(1);
                        }
                    }
                }
                catch (ArgumentOutOfRangeException aux)
                {
                    byte[] rawdata = new byte[35];
                    dataReaderObject.ReadBytes(rawdata);
                    //Debug.WriteLine(System.Text.Encoding.Unicode.GetString(rawdata, 0, rawdata.Length));
                    Debug.WriteLine(System.Text.Encoding.UTF8.GetString(rawdata, 0, rawdata.Length));
                    await Logging.WriteSystemLog("ReadAsync - {0} - {1}", aux.Message, System.Text.Encoding.UTF8.GetString(rawdata, 0, rawdata.Length));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Input stream:" + input);
                    Debug.WriteLine("ReadAsync: Exception thrown: " + ex.Message);
                    //string exMessage = "ReadAsync: Exception thrown - Message: " + ex.Message + "\n";
                    //exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                    //exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                    await Logging.WriteSystemLog("ReadAsync - {0}", ex.Message);
                    //return;
                }
            }
        }
       
        static ValueSet sentData = new ValueSet();
        
        
        //--------------------------Send data to bkNstApplication---------------------------------
        private static async Task sendToNstApp(string data)
        {
            sentData.Add("Message", data);
            sentData.Add("Receiver", "bkNstApplication");
            sentData.Add("Sender", "bkPortManager");
            await IPCservice.SendMessageAsync(sentData);
            sentData.Clear();
        }

        
        //-------------------------- Write command to USB port-------------------------------------
        public static async Task WriteAsync(SerialDevice sp, string message)
        {
        try
        {            
            // Create cancellation token object to close I/O operations when closing the device
            WriteCancellationTokenSource = new CancellationTokenSource();

            dataWriterObject = new DataWriter(sp.OutputStream);

            Task<UInt32> storeAsyncTask;
            dataWriterObject.WriteString(message);
            
            // Cancellation Token will be used so we can stop the task operation explicitly
            WriteCancellationTokenSource.Token.ThrowIfCancellationRequested();
           
            // The completion function should still be called so that we can properly handle a canceled task
            storeAsyncTask = dataWriterObject.StoreAsync().AsTask(WriteCancellationTokenSource.Token);

            UInt32 bytesWritten = await storeAsyncTask;
            if (bytesWritten > 0)
            {
                Debug.WriteLine("WRITTEN");
                await Logging.WriteDebugLog("Command sent to serial port: {0}", message);


            }
                // To be defined: open dataWriter only once
                dataWriterObject.DetachStream();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("WriteAsync: Exception thrown: " + ex.Message);
                //string exMessage = "WriteAsync: Exception thrown - Message: " + ex.Message + "\n";
                //exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                //exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                await Logging.WriteSystemLog("WriteAsync - {0}", ex.Message);
            }
        }

        //-------------------------- processNstCommandReceived-----------------------------------------
        public static async Task processNstCommandReceived(object in_object)
        {
            try
            {
                string command = in_object.ToString();
                Debug.WriteLine("--- Port Manager : COMMAND RECEIVED: " + command);

                // Convert string to NstMessage to obtain IpNode and eventually through db the CMG_Unique id

                NstMessage nstCommand = new NstMessage(command);
                
                string ipnode = nstCommand.Ipnode;
                string cmgUniqueId;
                if (nstCommand.Command.Equals(NstMessage.HBE_COMMAND) || nstCommand.Command.Equals(NstMessage.RSG_COMMAND))
                {
                    cmgUniqueId = await dbGetCMGUniqueID(ipnode, "CMG");
                }
                else
                {
                    cmgUniqueId = await dbGetCMGUniqueID(ipnode, "CMD");
                }

                SerialDevice sp = null;
                spPerCMG.TryGetValue(cmgUniqueId, out sp);

                string nstCommandInBytes = await convertNstMessageToBytes(nstCommand);
                // NEWLINE character is needed
                nstCommandInBytes += "\n";
                await Logging.WriteDebugLog("Command received from cloud: {0}", nstCommandInBytes);
                Debug.WriteLine("Before write async");

                await WriteAsync(sp, nstCommandInBytes);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("processNstCommandReceived: Exception thrown: " + ex.Message);
                //string exMessage = "processNstCommandReceived: Exception thrown - Message: " + ex.Message + "\n";
                //exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                //exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                await Logging.WriteSystemLog("processNstCommandReceived - {0}", ex.Message);

            }
        }

        //-------------------------- Converts NSTMessage to Bytes ---------------------------------------//
        public async static Task<string> convertNstMessageToBytes(comNST.NstMessage receivedCommand)
        {
            try
            {
                Debug.WriteLine("CONVERT");
                if (receivedCommand.Value == 0)
                {
                    string nstMessageInbytes2 = receivedCommand.Command + receivedCommand.Token + receivedCommand.Tokenlength +
                                           receivedCommand.Ipnode + receivedCommand.Resource;
                    return nstMessageInbytes2;
                }
                else
                {
                    string nstMessageInbytes = receivedCommand.Command + receivedCommand.Token + receivedCommand.Tokenlength +
                                           receivedCommand.Ipnode + receivedCommand.Resource +
                                           receivedCommand.Value;
                    return nstMessageInbytes;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("convertNstMessageToBytes: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("convertNstMessageToBytes: Exception thrown: {0}", ex.Message);
                return null;
            }
        }

        // it gets the CMG_Unique_ID
        // if the command is sending to a CMG, the ipnode refers to CMG 
        //      and the CMG_Unique_ID is derived from CMGs table
        // if the command is sending to a CMD, the ipnode refers to CMD 
        //      and the CMG_Unique_ID is derived from CMGs table through CMDs table
        static private async Task<string> dbGetCMGUniqueID(string cmdIpnode,string commandDestination)
        {
            using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db"))               
            {
                Debug.WriteLine("dbGetCMGUniqueID");
                SqliteCommand selectCommand;
                //SqliteCommand cmd;
                SqliteDataReader query;
                string cmgId = null;
                try
                {
                    db.Open();

                    if (commandDestination.Equals("CMD"))
                    {
                        // CASE I: command is sending to CMD and ipnode refers to a CMD
                        selectCommand = new SqliteCommand("SELECT CMG_Unique_ID from CMGs where CMGID in (select CMGID from CMDs where CMD_IPV6_Address = @ipnode)", db);

                        selectCommand.Parameters.AddWithValue("@ipnode", cmdIpnode);
                        query = selectCommand.ExecuteReader();

                        while (query.Read())
                        {
                            cmgId = query.GetString(0);
                           // Debug.WriteLine("Gateway:" + query.GetString(0));
                        }
                    }
                    else if (commandDestination.Equals("CMG"))
                    {

                        // CASE II: command is sending to CMG (e.g HBE command) and ipnode refers to a CMD
                        selectCommand = new SqliteCommand("SELECT CMG_Unique_ID from CMGs where CMG_IPV6_Address  = @ipnode", db);

                        selectCommand.Parameters.AddWithValue("@ipnode", cmdIpnode);
                        query = selectCommand.ExecuteReader();

                        while (query.Read())
                        {
                            cmgId = query.GetString(0);
                            Debug.WriteLine("Gateway:" + query.GetString(0));
                        }

                    }
                }
                catch (SqliteException error)
                {
                    //Handle error
                    Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                    await Logging.WriteSystemLog("dbGetAllCMGs - {0} - {1}", "Problem while executing the sql command: ", error.Message);
                    return null;
                }

                db.Close();
                return cmgId;
            }

        }

        //---------------------Gets all CMGs connected to Gateway------------------------------------//
        private static async Task<string[]> dbGetAllCMGs()
        {
            using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db"))
            {
                Debug.WriteLine("dbGetAllCMGs");
                SqliteCommand selectCommand;
                //SqliteCommand cmd;
                SqliteDataReader query;
                string[] connectedCMGs = new string[8];
                try
                {
                    db.Open();

                    // ----------  Read CMG table -----------------
                    selectCommand = new SqliteCommand("SELECT CMG_Unique_ID from CMGs", db);
                    query = selectCommand.ExecuteReader();
                }
                catch (SqliteException error)
                {
                    Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                    await Logging.WriteSystemLog("dbGetAllCMGs - {0} - {1}", "Problem while executing the sql command: ", error.Message);
                    return null;
                }
                int i = 0;
                while (query.Read())
                {
                    //entries.Add("   CMG Id: " + query.GetString(0) + " GatewayID: " + query.GetString(1));
                    connectedCMGs[i] = query.GetString(0);
                    i++;
                }
                numOfCMGsDeclaredinDB = i;
                db.Close();
                return connectedCMGs;
            }
        }


       
        // ErrorReiceved Event Handler Method
        private static void portErrorReceived(object sender, ErrorReceivedEventArgs e)
        {
            Debug.WriteLine("sdfsdf");
            //throw new NotImplementedException();
        }


    } //end of class ReadWrite
} //end of namespace blPortManager

